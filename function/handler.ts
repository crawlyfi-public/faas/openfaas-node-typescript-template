// @ts-nocheck
import { App } from "./app";
module.exports = async (event, context) => {
  let app = new App();
  app.run(event.body);
  return context
    .status(200)
    .succeed("Completed")
}